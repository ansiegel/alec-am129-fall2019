! Student name: Jason Avery - Driver
! Student name: Alec Siegel - Navigator 
! Group name: 344-4




module trapeziodApprx
    implicit none
contains
     real function f(x)
              implicit none
              real, intent(in) ::x
              f = x**2 + 1
    end function f

    real function trapezoidFunc(a,b,n)
        real, intent(in):: a ,b 
        real :: delta, ab, returned
        !real, external :: f
        integer i,n
        returned = 0
        delta = (b - a)/real(n)
        ab = (f(b)+f(a))/2.
        do i =1, n-1
           returned = returned + f(a+i*delta)
        end do
        returned =delta*(returned + ab)
        trapezoidFunc = returned
    end function trapezoidFunc 

    subroutine trapezoidSub (a,b,trap,n)
         real , intent(in) ::a,b
         real , intent(out) :: trap
         real ::delta, ab, returned
         !real, external :: f
         integer i,n
         returned = 0
         delta = (b - a)/real(n)
         ab =(f(b)+f(a))/2
         do i =1, n-1
         trap = trap + f(a+i*delta)
         end do
        trap =(delta)*(trap + ab)
    end subroutine trapezoidSub

    subroutine trapezoidExact(a,b, trap)
        real, intent(in) ::a,b
        real, intent(out) :: trap
        trap = ((1./3.)*b**3 + b) - ((1./3.)*a**3+a)
    end subroutine trapezoidExact
end module trapeziodapprx 


program trapeziodal 

use trapeziodApprx 
implicit none

real :: trapFunc, trapSub, trapExact
integer n
n= 400
trapFunc = trapezoidFunc(0.0,1.0,n)

call trapezoidSub(0.0, 1.0, trapSub, n)

call trapezoidExact(0.0,1.0, trapExact)
    print*, "[1] Trapezoidal in function   =", trapFunc 
    print*, "[2] Trapezoidal in subroutine =", trapSub 
    print*, "[3] Exact integration         =", trapExact 
    print*, "[4] Error in function         =", trapExact - trapFunc 
    print*, "[5] Error in subroutine       =", trapExact - trapSub 
end program trapeziodal 
