"""
Template file for hw6:

Directory structure:

   hw6/Prob2/code/
               |-- newton_rootFinder/RootFinder.F90
               |                     findRootMethod_module.F90
               |                     makefile
               |                     read_initFile_module.F90
               |                     definition.h
               |                     ftn_module.F90
               |                     output_module.F90
               |                     setup_module.F90
			   |                    (excluding rootFinder.init)
               |
               |-- pyRun/pyRun_rootFinder_prob2.py
       

"""
import os
import sys
from matplotlib import pyplot as  plt
import numpy as np
def make_make():
	path = '../newton_rootFinder'
	os.chdir(path)
	if(os.path.exists('rootFinder.exe')):
		os.system('make clean')
	
	os.system('make')


def runtimeParameters_init(threshold, init_guess, filetype):
	programName = 'rootFinder.init'
	i = 0
	while(os.path.exists(programName)):
		i = i +1
		programName = 'rootFinder.init.{0}'.format(i)
	
	command = 'mv rootFinder.init' + " " + programName
	os.system(command)
	

	programName = 'rootFinder.init'
	f = open(programName, 'w')
	f.write("run_name\t 'newton'\n")
	f.write("method_type\t 'newton'\n")
	f.write('x_beg\t          -10.0\n')
	f.write('x_end\t          3.0\n')
	f.write('max_iter\t  10000\n')
	f.write('threshold\t  ' + str(threshold) + '\n')
	f.write('ftn_type\t  '+str(filetype)+'\n')
	f.write('init_guess\t  '+str(init_guess)+'\n')
	f.write('multiplicity\t  4\n')
	f.close()
	run_rootFinder()
	plot_data(threshold, init_guess, filetype, i+1)




def run_rootFinder():
	#path = '../newton_rootFinder'
	#os.chdir(path)
	os.system('./rootFinder.exe')
	


def plot_data(threshold, init_guess, filetype,i):

	data = 'rootFinder_newton.dat'

	plt.interactive(False)
	graph_data = np.loadtxt(data)
	
	iteration = graph_data[:,0]
	solution = graph_data[:,1]
	error = graph_data[:,3]
	x_min =  min(iteration)
	x_max =  max(iteration)
	solMin = min(solution)
	errorMin = min(error)
	solMax = max(solution)
	errorMax = max(error)
	y_min =  min(solMin, errorMin ) - 0.05
	y_max =  max(solMax, errorMax ) + 0.05

	plt.figure()

    # (1) solution (y-axis) vs. iteration number (x-axis)
	plt.subplot(211)
	plt.tight_layout(h_pad = 10)
	plt.tight_layout(pad = -1)	
	plt.title('Solution vs Iteration: ' + 'Threshold =' + str(threshold) + ', ' 'Guess =' + str(init_guess)) 
	plt.xlabel('Iteration')
	plt.ylabel('Solution')
	plt.xlim(x_min, x_max)
	plt.ylim(y_min, y_max)
	plt.plot(iteration, solution, 'bo-')


    # (2) error (y-axis) vs. iteration number (x-axis)
	plt.subplot(212)
	plt.tight_layout(pad = 2)
	plt.title('Error vs Iteration: '+ 'Threshold =' + str(threshold) + ', ' 'Guess =' + str(init_guess)) 
	plt.xlabel('Iteration')
	plt.ylabel('Error')
	plt.xlim(x_min, x_max)
	plt.ylim(y_min, y_max)
	plt.plot(iteration, error, 'bo-')

  
	plt.savefig('results_'+str(filetype)+ '_'+str(threshold)+str(init_guess)+'.png')
	plt.show()
	if(i !=5):
		newFile = "rootFinder_newton.dat.{0}".format(i)
		command = 'mv rootFinder_newton.dat '+ newFile
		os.system(command)	
	

	

if __name__ == '__main__':

	make_make()
	runtimeParameters_init(1.e-8,-.12811,2)
	runtimeParameters_init(1.e-6,-.12811,2)
	runtimeParameters_init(1.e-4,-.12811,2)
	runtimeParameters_init(1.e-8,.9,2)
	runtimeParameters_init(1.e-8,-9,2)
