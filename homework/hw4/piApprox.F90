module piapprox

contains

    Subroutine piappx(n,f)
        implicit none
        real*8, intent(IN) :: n
        real*8, intent(OUT) :: f
        f = 16.**-n * &
            (4./(8.*n + 1.)- &
            2./(8.*n + 4.)- & 
            1./(8.*n + 5.)- &
            1./(8.*n + 6.))
    end subroutine piappx

end module piapprox