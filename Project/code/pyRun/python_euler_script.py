import os
from matplotlib import pyplot as  plt
import numpy as np
def make_make():
	path = '../fortran'
	os.chdir(path)
	if(os.path.exists('eulers.exe')):
		os.system('make clean')
	
	os.system('make')

def run_euler():
	os.system('./eulers.exe')


def plot_data(data):
	path = '../fortran'
	os.chdir(path)
	datafile = 'output_{:02d}.dat'.format(data)

	plt.interactive(False)
	graph_data = np.loadtxt(datafile)
	iteration = graph_data[:,0]
	numerical_data = graph_data[:,1]
	real_data = graph_data[:,2]
	error = 0	
	for i, num in enumerate(numerical_data):
		error += abs(real_data[i]-num)

	error = error*(1/data)
	plt.figure()
	plt.xlabel('t axis')
	plt.ylabel('y axis')
	plt.title("Error = " + str(error) + "   N = " + str(data))
	plt.plot(iteration, real_data, 'b', label = "Real solution")
	plt.plot(iteration, numerical_data, 'ro', linestyle = "dashed", label = "Numerical solutuon")
	plt.legend()
	plt.grid(True)
	path = '../pyRun'
	os.chdir(path)
	plt.savefig('results_{:02d}.png'.format(data))
	plt.show()
	

if __name__ == '__main__':
	make_make()
	run_euler();
	data_list = [8,16,32,64]
	for data in data_list:
		plot_data(data)
